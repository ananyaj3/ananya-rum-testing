// Compiled by ClojureScript 1.10.520 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__22243){
var map__22244 = p__22243;
var map__22244__$1 = (((((!((map__22244 == null))))?(((((map__22244.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22244.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22244):map__22244);
var m = map__22244__$1;
var n = cljs.core.get.call(null,map__22244__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__22244__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5457__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5457__auto__)){
var ns = temp__5457__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})());

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__22246_22278 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__22247_22279 = null;
var count__22248_22280 = (0);
var i__22249_22281 = (0);
while(true){
if((i__22249_22281 < count__22248_22280)){
var f_22282 = cljs.core._nth.call(null,chunk__22247_22279,i__22249_22281);
cljs.core.println.call(null,"  ",f_22282);


var G__22283 = seq__22246_22278;
var G__22284 = chunk__22247_22279;
var G__22285 = count__22248_22280;
var G__22286 = (i__22249_22281 + (1));
seq__22246_22278 = G__22283;
chunk__22247_22279 = G__22284;
count__22248_22280 = G__22285;
i__22249_22281 = G__22286;
continue;
} else {
var temp__5457__auto___22287 = cljs.core.seq.call(null,seq__22246_22278);
if(temp__5457__auto___22287){
var seq__22246_22288__$1 = temp__5457__auto___22287;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__22246_22288__$1)){
var c__4550__auto___22289 = cljs.core.chunk_first.call(null,seq__22246_22288__$1);
var G__22290 = cljs.core.chunk_rest.call(null,seq__22246_22288__$1);
var G__22291 = c__4550__auto___22289;
var G__22292 = cljs.core.count.call(null,c__4550__auto___22289);
var G__22293 = (0);
seq__22246_22278 = G__22290;
chunk__22247_22279 = G__22291;
count__22248_22280 = G__22292;
i__22249_22281 = G__22293;
continue;
} else {
var f_22294 = cljs.core.first.call(null,seq__22246_22288__$1);
cljs.core.println.call(null,"  ",f_22294);


var G__22295 = cljs.core.next.call(null,seq__22246_22288__$1);
var G__22296 = null;
var G__22297 = (0);
var G__22298 = (0);
seq__22246_22278 = G__22295;
chunk__22247_22279 = G__22296;
count__22248_22280 = G__22297;
i__22249_22281 = G__22298;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_22299 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_22299);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_22299)))?cljs.core.second.call(null,arglists_22299):arglists_22299));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Spec");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__22250_22300 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__22251_22301 = null;
var count__22252_22302 = (0);
var i__22253_22303 = (0);
while(true){
if((i__22253_22303 < count__22252_22302)){
var vec__22264_22304 = cljs.core._nth.call(null,chunk__22251_22301,i__22253_22303);
var name_22305 = cljs.core.nth.call(null,vec__22264_22304,(0),null);
var map__22267_22306 = cljs.core.nth.call(null,vec__22264_22304,(1),null);
var map__22267_22307__$1 = (((((!((map__22267_22306 == null))))?(((((map__22267_22306.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22267_22306.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22267_22306):map__22267_22306);
var doc_22308 = cljs.core.get.call(null,map__22267_22307__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_22309 = cljs.core.get.call(null,map__22267_22307__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_22305);

cljs.core.println.call(null," ",arglists_22309);

if(cljs.core.truth_(doc_22308)){
cljs.core.println.call(null," ",doc_22308);
} else {
}


var G__22310 = seq__22250_22300;
var G__22311 = chunk__22251_22301;
var G__22312 = count__22252_22302;
var G__22313 = (i__22253_22303 + (1));
seq__22250_22300 = G__22310;
chunk__22251_22301 = G__22311;
count__22252_22302 = G__22312;
i__22253_22303 = G__22313;
continue;
} else {
var temp__5457__auto___22314 = cljs.core.seq.call(null,seq__22250_22300);
if(temp__5457__auto___22314){
var seq__22250_22315__$1 = temp__5457__auto___22314;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__22250_22315__$1)){
var c__4550__auto___22316 = cljs.core.chunk_first.call(null,seq__22250_22315__$1);
var G__22317 = cljs.core.chunk_rest.call(null,seq__22250_22315__$1);
var G__22318 = c__4550__auto___22316;
var G__22319 = cljs.core.count.call(null,c__4550__auto___22316);
var G__22320 = (0);
seq__22250_22300 = G__22317;
chunk__22251_22301 = G__22318;
count__22252_22302 = G__22319;
i__22253_22303 = G__22320;
continue;
} else {
var vec__22269_22321 = cljs.core.first.call(null,seq__22250_22315__$1);
var name_22322 = cljs.core.nth.call(null,vec__22269_22321,(0),null);
var map__22272_22323 = cljs.core.nth.call(null,vec__22269_22321,(1),null);
var map__22272_22324__$1 = (((((!((map__22272_22323 == null))))?(((((map__22272_22323.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22272_22323.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22272_22323):map__22272_22323);
var doc_22325 = cljs.core.get.call(null,map__22272_22324__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_22326 = cljs.core.get.call(null,map__22272_22324__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_22322);

cljs.core.println.call(null," ",arglists_22326);

if(cljs.core.truth_(doc_22325)){
cljs.core.println.call(null," ",doc_22325);
} else {
}


var G__22327 = cljs.core.next.call(null,seq__22250_22315__$1);
var G__22328 = null;
var G__22329 = (0);
var G__22330 = (0);
seq__22250_22300 = G__22327;
chunk__22251_22301 = G__22328;
count__22252_22302 = G__22329;
i__22253_22303 = G__22330;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5457__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n)),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5457__auto__)){
var fnspec = temp__5457__auto__;
cljs.core.print.call(null,"Spec");

var seq__22274 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__22275 = null;
var count__22276 = (0);
var i__22277 = (0);
while(true){
if((i__22277 < count__22276)){
var role = cljs.core._nth.call(null,chunk__22275,i__22277);
var temp__5457__auto___22331__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___22331__$1)){
var spec_22332 = temp__5457__auto___22331__$1;
cljs.core.print.call(null,["\n ",cljs.core.name.call(null,role),":"].join(''),cljs.spec.alpha.describe.call(null,spec_22332));
} else {
}


var G__22333 = seq__22274;
var G__22334 = chunk__22275;
var G__22335 = count__22276;
var G__22336 = (i__22277 + (1));
seq__22274 = G__22333;
chunk__22275 = G__22334;
count__22276 = G__22335;
i__22277 = G__22336;
continue;
} else {
var temp__5457__auto____$1 = cljs.core.seq.call(null,seq__22274);
if(temp__5457__auto____$1){
var seq__22274__$1 = temp__5457__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__22274__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__22274__$1);
var G__22337 = cljs.core.chunk_rest.call(null,seq__22274__$1);
var G__22338 = c__4550__auto__;
var G__22339 = cljs.core.count.call(null,c__4550__auto__);
var G__22340 = (0);
seq__22274 = G__22337;
chunk__22275 = G__22338;
count__22276 = G__22339;
i__22277 = G__22340;
continue;
} else {
var role = cljs.core.first.call(null,seq__22274__$1);
var temp__5457__auto___22341__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___22341__$2)){
var spec_22342 = temp__5457__auto___22341__$2;
cljs.core.print.call(null,["\n ",cljs.core.name.call(null,role),":"].join(''),cljs.spec.alpha.describe.call(null,spec_22342));
} else {
}


var G__22343 = cljs.core.next.call(null,seq__22274__$1);
var G__22344 = null;
var G__22345 = (0);
var G__22346 = (0);
seq__22274 = G__22343;
chunk__22275 = G__22344;
count__22276 = G__22345;
i__22277 = G__22346;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5457__auto__ = cljs.core.ex_message.call(null,t);
if(cljs.core.truth_(temp__5457__auto__)){
var msg = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = cljs.core.ex_data.call(null,t);
if(cljs.core.truth_(temp__5457__auto__)){
var ed = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})());
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__22347 = cljs.core.conj.call(null,via,t);
var G__22348 = cljs.core.ex_cause.call(null,t);
via = G__22347;
t = G__22348;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek.call(null,via);
return cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec.call(null,cljs.core.map.call(null,base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5457__auto__ = cljs.core.ex_message.call(null,root);
if(cljs.core.truth_(temp__5457__auto__)){
var root_msg = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = cljs.core.ex_data.call(null,root);
if(cljs.core.truth_(temp__5457__auto__)){
var data = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5457__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data.call(null,o));
if(cljs.core.truth_(temp__5457__auto__)){
var phase = temp__5457__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})());
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__22351 = datafied_throwable;
var map__22351__$1 = (((((!((map__22351 == null))))?(((((map__22351.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22351.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22351):map__22351);
var via = cljs.core.get.call(null,map__22351__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.call(null,map__22351__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.call(null,map__22351__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__22352 = cljs.core.last.call(null,via);
var map__22352__$1 = (((((!((map__22352 == null))))?(((((map__22352.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22352.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22352):map__22352);
var type = cljs.core.get.call(null,map__22352__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.call(null,map__22352__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.call(null,map__22352__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__22353 = data;
var map__22353__$1 = (((((!((map__22353 == null))))?(((((map__22353.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22353.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22353):map__22353);
var problems = cljs.core.get.call(null,map__22353__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.call(null,map__22353__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.call(null,map__22353__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__22354 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,via));
var map__22354__$1 = (((((!((map__22354 == null))))?(((((map__22354.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22354.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22354):map__22354);
var top_data = map__22354__$1;
var source = cljs.core.get.call(null,map__22354__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.call(null,(function (){var G__22359 = phase;
var G__22359__$1 = (((G__22359 instanceof cljs.core.Keyword))?G__22359.fqn:null);
switch (G__22359__$1) {
case "read-source":
var map__22360 = data;
var map__22360__$1 = (((((!((map__22360 == null))))?(((((map__22360.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22360.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22360):map__22360);
var line = cljs.core.get.call(null,map__22360__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.call(null,map__22360__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__22362 = cljs.core.merge.call(null,new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,via)),top_data);
var G__22362__$1 = (cljs.core.truth_(source)?cljs.core.assoc.call(null,G__22362,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__22362);
var G__22362__$2 = (cljs.core.truth_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,source))?cljs.core.dissoc.call(null,G__22362__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__22362__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.call(null,G__22362__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__22362__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__22363 = top_data;
var G__22363__$1 = (cljs.core.truth_(source)?cljs.core.assoc.call(null,G__22363,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__22363);
var G__22363__$2 = (cljs.core.truth_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,source))?cljs.core.dissoc.call(null,G__22363__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__22363__$1);
var G__22363__$3 = (cljs.core.truth_(type)?cljs.core.assoc.call(null,G__22363__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__22363__$2);
var G__22363__$4 = (cljs.core.truth_(message)?cljs.core.assoc.call(null,G__22363__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__22363__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.call(null,G__22363__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__22363__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__22364 = cljs.core.first.call(null,trace);
var source__$1 = cljs.core.nth.call(null,vec__22364,(0),null);
var method = cljs.core.nth.call(null,vec__22364,(1),null);
var file = cljs.core.nth.call(null,vec__22364,(2),null);
var line = cljs.core.nth.call(null,vec__22364,(3),null);
var G__22367 = top_data;
var G__22367__$1 = (cljs.core.truth_(line)?cljs.core.assoc.call(null,G__22367,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__22367);
var G__22367__$2 = (cljs.core.truth_(file)?cljs.core.assoc.call(null,G__22367__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__22367__$1);
var G__22367__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.call(null,G__22367__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__22367__$2);
var G__22367__$4 = (cljs.core.truth_(type)?cljs.core.assoc.call(null,G__22367__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__22367__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.call(null,G__22367__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__22367__$4;
}

break;
case "execution":
var vec__22368 = cljs.core.first.call(null,trace);
var source__$1 = cljs.core.nth.call(null,vec__22368,(0),null);
var method = cljs.core.nth.call(null,vec__22368,(1),null);
var file = cljs.core.nth.call(null,vec__22368,(2),null);
var line = cljs.core.nth.call(null,vec__22368,(3),null);
var file__$1 = cljs.core.first.call(null,cljs.core.remove.call(null,((function (vec__22368,source__$1,method,file,line,G__22359,G__22359__$1,map__22351,map__22351__$1,via,trace,phase,map__22352,map__22352__$1,type,message,data,map__22353,map__22353__$1,problems,fn,caller,map__22354,map__22354__$1,top_data,source){
return (function (p1__22350_SHARP_){
var or__4131__auto__ = (p1__22350_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null).call(null,p1__22350_SHARP_);
}
});})(vec__22368,source__$1,method,file,line,G__22359,G__22359__$1,map__22351,map__22351__$1,via,trace,phase,map__22352,map__22352__$1,type,message,data,map__22353,map__22353__$1,problems,fn,caller,map__22354,map__22354__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__22371 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__22371__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.call(null,G__22371,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__22371);
var G__22371__$2 = (cljs.core.truth_(message)?cljs.core.assoc.call(null,G__22371__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__22371__$1);
var G__22371__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.call(null,G__22371__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__22371__$2);
var G__22371__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.call(null,G__22371__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__22371__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.call(null,G__22371__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__22371__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__22359__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__22375){
var map__22376 = p__22375;
var map__22376__$1 = (((((!((map__22376 == null))))?(((((map__22376.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22376.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22376):map__22376);
var triage_data = map__22376__$1;
var phase = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.call(null,map__22376__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name.call(null,(function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__22378 = phase;
var G__22378__$1 = (((G__22378 instanceof cljs.core.Keyword))?G__22378.fqn:null);
switch (G__22378__$1) {
case "read-source":
return format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause);

break;
case "macro-syntax-check":
return format.call(null,"Syntax error macroexpanding %sat (%s).\n%s",(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,(cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__22379_22388 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__22380_22389 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__22381_22390 = true;
var _STAR_print_fn_STAR__temp_val__22382_22391 = ((function (_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22381_22390;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22382_22391;

try{cljs.spec.alpha.explain_out.call(null,cljs.core.update.call(null,spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,_STAR_print_fn_STAR__temp_val__22382_22391,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.call(null,((function (_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,_STAR_print_fn_STAR__temp_val__22382_22391,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__22373_SHARP_){
return cljs.core.dissoc.call(null,p1__22373_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,_STAR_print_fn_STAR__temp_val__22382_22391,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__22379_22388,_STAR_print_fn_STAR__orig_val__22380_22389,_STAR_print_newline_STAR__temp_val__22381_22390,_STAR_print_fn_STAR__temp_val__22382_22391,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22380_22389;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22379_22388;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():format.call(null,"%s\n",cause)));

break;
case "macroexpansion":
return format.call(null,"Unexpected error%s macroexpanding %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "compile-syntax-check":
return format.call(null,"Syntax error%s compiling %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "compilation":
return format.call(null,"Unexpected error%s compiling %sat (%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);

break;
case "read-eval-result":
return format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause);

break;
case "print-eval-result":
return format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause);

break;
case "execution":
if(cljs.core.truth_(spec)){
return format.call(null,"Execution error - invalid arguments to %s at (%s).\n%s",symbol,loc,(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__22383_22392 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__22384_22393 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__22385_22394 = true;
var _STAR_print_fn_STAR__temp_val__22386_22395 = ((function (_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__22385_22394;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__22386_22395;

try{cljs.spec.alpha.explain_out.call(null,cljs.core.update.call(null,spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,_STAR_print_fn_STAR__temp_val__22386_22395,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.call(null,((function (_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,_STAR_print_fn_STAR__temp_val__22386_22395,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__22374_SHARP_){
return cljs.core.dissoc.call(null,p1__22374_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,_STAR_print_fn_STAR__temp_val__22386_22395,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__22383_22392,_STAR_print_fn_STAR__orig_val__22384_22393,_STAR_print_newline_STAR__temp_val__22385_22394,_STAR_print_fn_STAR__temp_val__22386_22395,sb__4661__auto__,G__22378,G__22378__$1,loc,class_name,simple_class,cause_type,format,map__22376,map__22376__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__22384_22393;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__22383_22392;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})());
} else {
return format.call(null,"Execution error%s at %s(%s).\n%s\n",cause_type,(cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):""),loc,cause);
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__22378__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str.call(null,cljs.repl.ex_triage.call(null,cljs.repl.Error__GT_map.call(null,error)));
});

//# sourceMappingURL=repl.js.map
