// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum_test.core_test');
goog.require('cljs.core');
goog.require('cljs.test');
rum_test.core_test.multiply_test = (function rum_test$core_test$multiply_test(){
return cljs.test.test_var.call(null,rum_test.core_test.multiply_test.cljs$lang$var);
});
rum_test.core_test.multiply_test.cljs$lang$test = (function (){
try{var values__13151__auto__ = (new cljs.core.List(null,((1) * (2)),(new cljs.core.List(null,((1) + (1)),null,(1),null)),(2),null));
var result__13152__auto__ = cljs.core.apply.call(null,cljs.core._EQ_,values__13151__auto__);
if(cljs.core.truth_(result__13152__auto__)){
cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"pass","pass",1574159993),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),cljs.core.list(new cljs.core.Symbol(null,"*","*",345799209,null),(1),(2)),cljs.core.list(new cljs.core.Symbol(null,"+","+",-740910886,null),(1),(1))),new cljs.core.Keyword(null,"actual","actual",107306363),cljs.core.cons.call(null,cljs.core._EQ_,values__13151__auto__),new cljs.core.Keyword(null,"message","message",-406056002),null], null));
} else {
cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"fail","fail",1706214930),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),cljs.core.list(new cljs.core.Symbol(null,"*","*",345799209,null),(1),(2)),cljs.core.list(new cljs.core.Symbol(null,"+","+",-740910886,null),(1),(1))),new cljs.core.Keyword(null,"actual","actual",107306363),(new cljs.core.List(null,new cljs.core.Symbol(null,"not","not",1044554643,null),(new cljs.core.List(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"=","=",-1501502141,null),values__13151__auto__),null,(1),null)),(2),null)),new cljs.core.Keyword(null,"message","message",-406056002),null], null));
}

return result__13152__auto__;
}catch (e23006){var t__13196__auto__ = e23006;
return cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"error","error",-978969032),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),cljs.core.list(new cljs.core.Symbol(null,"*","*",345799209,null),(1),(2)),cljs.core.list(new cljs.core.Symbol(null,"+","+",-740910886,null),(1),(1))),new cljs.core.Keyword(null,"actual","actual",107306363),t__13196__auto__,new cljs.core.Keyword(null,"message","message",-406056002),null], null));
}});

rum_test.core_test.multiply_test.cljs$lang$var = new cljs.core.Var(function(){return rum_test.core_test.multiply_test;},new cljs.core.Symbol("rum-test.core-test","multiply-test","rum-test.core-test/multiply-test",909960887,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"rum-test.core-test","rum-test.core-test",1156686040,null),new cljs.core.Symbol(null,"multiply-test","multiply-test",373518316,null),"test/rum_test/core_test.cljs",23,1,5,5,cljs.core.List.EMPTY,null,(cljs.core.truth_(rum_test.core_test.multiply_test)?rum_test.core_test.multiply_test.cljs$lang$test:null)]));

//# sourceMappingURL=core_test.js.map
