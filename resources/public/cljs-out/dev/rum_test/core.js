// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum_test.core');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('rum.core');
goog.require('jayq.core');
goog.require('sablono.util');
if((typeof rum_test !== 'undefined') && (typeof rum_test.core !== 'undefined') && (typeof rum_test.core.app_state !== 'undefined')){
} else {
rum_test.core.app_state = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"text","text",-1790561697),"Ananya's Rum Testing",new cljs.core.Keyword(null,"counter","counter",804008177),(0)], null));
}
if((typeof rum_test !== 'undefined') && (typeof rum_test.core !== 'undefined') && (typeof rum_test.core.is_element_dropped_QMARK_ !== 'undefined')){
} else {
rum_test.core.is_element_dropped_QMARK_ = cljs.core.atom.call(null,false);
}
rum_test.core.attrs = (function rum_test$core$attrs(a){
return cljs.core.clj__GT_js.call(null,sablono.util.html_to_dom_attrs.call(null,a));
});
rum_test.core.get_app_element = (function rum_test$core$get_app_element(){
return goog.dom.getElement("app");
});
rum_test.core.add_like = (function rum_test$core$add_like(){
return cljs.core.swap_BANG_.call(null,rum_test.core.app_state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"counter","counter",804008177)], null),cljs.core.inc);
});
rum_test.core.person = rum.core.build_defc.call(null,(function (name,likes){
return React.createElement("div",({"onClick": (function (){
return rum_test.core.add_like.call(null);
}), "className": "person"}),sablono.interpreter.interpret.call(null,["Welcome to the app! ","Click me! ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(name)," is liked ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(likes)," times "].join('')));
}),null,"person");
rum_test.core.like_counter = rum.core.build_defc.call(null,(function (){
return React.createElement("div",null,sablono.interpreter.interpret.call(null,rum_test.core.person.call(null,"Ananya",new cljs.core.Keyword(null,"counter","counter",804008177).cljs$core$IFn$_invoke$arity$1(rum.core.react.call(null,rum_test.core.app_state)))));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [rum.core.reactive], null),"like-counter");
rum_test.core.tile = rum.core.build_defc.call(null,(function (text,number){
return React.createElement("div",({"id": number, "className": "tile"}),sablono.interpreter.interpret.call(null,text));
}),null,"tile");
rum_test.core.tiles = rum.core.build_defc.call(null,(function (){
return React.createElement("div",({"className": "tiles"}),sablono.interpreter.interpret.call(null,rum_test.core.tile.call(null,"Drop the red box in here",(1))));
}),null,"tiles");
rum_test.core.dropped_message = rum.core.build_defc.call(null,(function (){
return React.createElement("div",null,sablono.interpreter.interpret.call(null,["Has an element been dropped? ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(rum.core.react.call(null,rum_test.core.is_element_dropped_QMARK_))].join('')));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [rum.core.reactive], null),"dropped-message");
rum_test.core.card = rum.core.build_defc.call(null,(function (number){
return React.createElement("div",({"data-number": number, "id": number, "className": "card"}));
}),null,"card");
rum_test.core.cards = rum.core.build_defc.call(null,(function (){
return React.createElement("div",({"className": "cards"}),sablono.interpreter.interpret.call(null,rum_test.core.card.call(null,(1))));
}),null,"cards");
rum_test.core.content = rum.core.build_defc.call(null,(function (){
return React.createElement("div",null,sablono.interpreter.interpret.call(null,rum_test.core.like_counter.call(null)),sablono.interpreter.interpret.call(null,rum_test.core.tiles.call(null)),sablono.interpreter.interpret.call(null,rum_test.core.cards.call(null)),sablono.interpreter.interpret.call(null,rum_test.core.dropped_message.call(null)));
}),null,"content");
rum_test.core.mount = (function rum_test$core$mount(el){
return rum.core.mount.call(null,rum_test.core.content.call(null),el);
});
rum_test.core.mount_app_element = (function rum_test$core$mount_app_element(){
var temp__5457__auto__ = rum_test.core.get_app_element.call(null);
if(cljs.core.truth_(temp__5457__auto__)){
var el = temp__5457__auto__;
return rum_test.core.mount.call(null,el);
} else {
return null;
}
});
rum_test.core.make_draggable = (function rum_test$core$make_draggable(){
return jayq.core.$.call(null,".card").draggable(rum_test.core.attrs.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"revert","revert",-983985933),true,new cljs.core.Keyword(null,"cursor","cursor",1011937484),"move"], null)));
});
rum_test.core.handle_drop = (function rum_test$core$handle_drop(event,ui){
var draggable_id = jayq.core.data.call(null,ui.draggable,"number");
cljs.core.println.call(null,"Dropping element with id",draggable_id);

cljs.core.reset_BANG_.call(null,rum_test.core.is_element_dropped_QMARK_,true);

ui.draggable.draggable("disable");

jayq.core.$.call(null,["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(event.target.id)].join('')).droppable("disable");

return ui.draggable.position(rum_test.core.attrs.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"of","of",-2075414212),jayq.core.$.call(null,["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(event.target.id)].join('')),new cljs.core.Keyword(null,"my","my",-1055703269),"left top",new cljs.core.Keyword(null,"at","at",1476951349),"left top"], null)));
});
rum_test.core.start_dragging = (function rum_test$core$start_dragging(event,ui){
return cljs.core.reset_BANG_.call(null,rum_test.core.is_element_dropped_QMARK_,false);
});
rum_test.core.make_droppable = (function rum_test$core$make_droppable(){
return jayq.core.$.call(null,".tile").droppable(rum_test.core.attrs.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"hoverClass","hoverClass",-1073668821),"hovered-tile",new cljs.core.Keyword(null,"drop","drop",364481611),rum_test.core.handle_drop,new cljs.core.Keyword(null,"activate","activate",441219614),rum_test.core.start_dragging], null)));
});
rum_test.core.on_reload = (function rum_test$core$on_reload(){
rum_test.core.mount_app_element.call(null);

rum_test.core.make_draggable.call(null);

return rum_test.core.make_droppable.call(null);
});

//# sourceMappingURL=core.js.map
