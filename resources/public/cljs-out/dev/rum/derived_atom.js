// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum.derived_atom');
goog.require('cljs.core');
rum.derived_atom.derived_atom = (function rum$derived_atom$derived_atom(var_args){
var G__8587 = arguments.length;
switch (G__8587) {
case 3:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$3 = (function (refs,key,f){
return rum.derived_atom.derived_atom.call(null,refs,key,f,cljs.core.PersistentArrayMap.EMPTY);
});

rum.derived_atom.derived_atom.cljs$core$IFn$_invoke$arity$4 = (function (refs,key,f,opts){
var map__8588 = opts;
var map__8588__$1 = (((((!((map__8588 == null))))?(((((map__8588.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__8588.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__8588):map__8588);
var ref = cljs.core.get.call(null,map__8588__$1,new cljs.core.Keyword(null,"ref","ref",1289896967));
var check_equals_QMARK_ = cljs.core.get.call(null,map__8588__$1,new cljs.core.Keyword(null,"check-equals?","check-equals?",-2005755315),true);
var recalc = (function (){var G__8590 = cljs.core.count.call(null,refs);
switch (G__8590) {
case (1):
var vec__8591 = refs;
var a = cljs.core.nth.call(null,vec__8591,(0),null);
return ((function (vec__8591,a,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a));
});
;})(vec__8591,a,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_))

break;
case (2):
var vec__8594 = refs;
var a = cljs.core.nth.call(null,vec__8594,(0),null);
var b = cljs.core.nth.call(null,vec__8594,(1),null);
return ((function (vec__8594,a,b,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a),cljs.core.deref.call(null,b));
});
;})(vec__8594,a,b,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_))

break;
case (3):
var vec__8597 = refs;
var a = cljs.core.nth.call(null,vec__8597,(0),null);
var b = cljs.core.nth.call(null,vec__8597,(1),null);
var c = cljs.core.nth.call(null,vec__8597,(2),null);
return ((function (vec__8597,a,b,c,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_){
return (function (){
return f.call(null,cljs.core.deref.call(null,a),cljs.core.deref.call(null,b),cljs.core.deref.call(null,c));
});
;})(vec__8597,a,b,c,G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_))

break;
default:
return ((function (G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_){
return (function (){
return cljs.core.apply.call(null,f,cljs.core.map.call(null,cljs.core.deref,refs));
});
;})(G__8590,map__8588,map__8588__$1,ref,check_equals_QMARK_))

}
})();
var sink = (cljs.core.truth_(ref)?(function (){var G__8600 = ref;
cljs.core.reset_BANG_.call(null,G__8600,recalc.call(null));

return G__8600;
})():cljs.core.atom.call(null,recalc.call(null)));
var watch = (cljs.core.truth_(check_equals_QMARK_)?((function (map__8588,map__8588__$1,ref,check_equals_QMARK_,recalc,sink){
return (function (_,___$1,___$2,___$3){
var new_val = recalc.call(null);
if(cljs.core.not_EQ_.call(null,cljs.core.deref.call(null,sink),new_val)){
return cljs.core.reset_BANG_.call(null,sink,new_val);
} else {
return null;
}
});})(map__8588,map__8588__$1,ref,check_equals_QMARK_,recalc,sink))
:((function (map__8588,map__8588__$1,ref,check_equals_QMARK_,recalc,sink){
return (function (_,___$1,___$2,___$3){
return cljs.core.reset_BANG_.call(null,sink,recalc.call(null));
});})(map__8588,map__8588__$1,ref,check_equals_QMARK_,recalc,sink))
);
var seq__8601_8607 = cljs.core.seq.call(null,refs);
var chunk__8602_8608 = null;
var count__8603_8609 = (0);
var i__8604_8610 = (0);
while(true){
if((i__8604_8610 < count__8603_8609)){
var ref_8611__$1 = cljs.core._nth.call(null,chunk__8602_8608,i__8604_8610);
cljs.core.add_watch.call(null,ref_8611__$1,key,watch);


var G__8612 = seq__8601_8607;
var G__8613 = chunk__8602_8608;
var G__8614 = count__8603_8609;
var G__8615 = (i__8604_8610 + (1));
seq__8601_8607 = G__8612;
chunk__8602_8608 = G__8613;
count__8603_8609 = G__8614;
i__8604_8610 = G__8615;
continue;
} else {
var temp__5457__auto___8616 = cljs.core.seq.call(null,seq__8601_8607);
if(temp__5457__auto___8616){
var seq__8601_8617__$1 = temp__5457__auto___8616;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__8601_8617__$1)){
var c__4550__auto___8618 = cljs.core.chunk_first.call(null,seq__8601_8617__$1);
var G__8619 = cljs.core.chunk_rest.call(null,seq__8601_8617__$1);
var G__8620 = c__4550__auto___8618;
var G__8621 = cljs.core.count.call(null,c__4550__auto___8618);
var G__8622 = (0);
seq__8601_8607 = G__8619;
chunk__8602_8608 = G__8620;
count__8603_8609 = G__8621;
i__8604_8610 = G__8622;
continue;
} else {
var ref_8623__$1 = cljs.core.first.call(null,seq__8601_8617__$1);
cljs.core.add_watch.call(null,ref_8623__$1,key,watch);


var G__8624 = cljs.core.next.call(null,seq__8601_8617__$1);
var G__8625 = null;
var G__8626 = (0);
var G__8627 = (0);
seq__8601_8607 = G__8624;
chunk__8602_8608 = G__8625;
count__8603_8609 = G__8626;
i__8604_8610 = G__8627;
continue;
}
} else {
}
}
break;
}

return sink;
});

rum.derived_atom.derived_atom.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=derived_atom.js.map
