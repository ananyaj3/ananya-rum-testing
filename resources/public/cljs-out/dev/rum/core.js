// Compiled by ClojureScript 1.10.520 {}
goog.provide('rum.core');
goog.require('cljs.core');
goog.require('cljsjs.react');
goog.require('cljsjs.react.dom');
goog.require('goog.object');
goog.require('sablono.core');
goog.require('rum.cursor');
goog.require('rum.util');
goog.require('rum.derived_atom');
/**
 * Given React component, returns Rum state associated with it
 */
rum.core.state = (function rum$core$state(comp){
return goog.object.get(comp.state,":rum/state");
});
rum.core.extend_BANG_ = (function rum$core$extend_BANG_(obj,props){
var seq__11008 = cljs.core.seq.call(null,props);
var chunk__11010 = null;
var count__11011 = (0);
var i__11012 = (0);
while(true){
if((i__11012 < count__11011)){
var vec__11020 = cljs.core._nth.call(null,chunk__11010,i__11012);
var k = cljs.core.nth.call(null,vec__11020,(0),null);
var v = cljs.core.nth.call(null,vec__11020,(1),null);
if((!((v == null)))){
goog.object.set(obj,cljs.core.name.call(null,k),cljs.core.clj__GT_js.call(null,v));


var G__11026 = seq__11008;
var G__11027 = chunk__11010;
var G__11028 = count__11011;
var G__11029 = (i__11012 + (1));
seq__11008 = G__11026;
chunk__11010 = G__11027;
count__11011 = G__11028;
i__11012 = G__11029;
continue;
} else {
var G__11030 = seq__11008;
var G__11031 = chunk__11010;
var G__11032 = count__11011;
var G__11033 = (i__11012 + (1));
seq__11008 = G__11030;
chunk__11010 = G__11031;
count__11011 = G__11032;
i__11012 = G__11033;
continue;
}
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__11008);
if(temp__5457__auto__){
var seq__11008__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11008__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__11008__$1);
var G__11034 = cljs.core.chunk_rest.call(null,seq__11008__$1);
var G__11035 = c__4550__auto__;
var G__11036 = cljs.core.count.call(null,c__4550__auto__);
var G__11037 = (0);
seq__11008 = G__11034;
chunk__11010 = G__11035;
count__11011 = G__11036;
i__11012 = G__11037;
continue;
} else {
var vec__11023 = cljs.core.first.call(null,seq__11008__$1);
var k = cljs.core.nth.call(null,vec__11023,(0),null);
var v = cljs.core.nth.call(null,vec__11023,(1),null);
if((!((v == null)))){
goog.object.set(obj,cljs.core.name.call(null,k),cljs.core.clj__GT_js.call(null,v));


var G__11038 = cljs.core.next.call(null,seq__11008__$1);
var G__11039 = null;
var G__11040 = (0);
var G__11041 = (0);
seq__11008 = G__11038;
chunk__11010 = G__11039;
count__11011 = G__11040;
i__11012 = G__11041;
continue;
} else {
var G__11042 = cljs.core.next.call(null,seq__11008__$1);
var G__11043 = null;
var G__11044 = (0);
var G__11045 = (0);
seq__11008 = G__11042;
chunk__11010 = G__11043;
count__11011 = G__11044;
i__11012 = G__11045;
continue;
}
}
} else {
return null;
}
}
break;
}
});
rum.core.build_class = (function rum$core$build_class(render,mixins,display_name){
var init = rum.util.collect.call(null,new cljs.core.Keyword(null,"init","init",-1875481434),mixins);
var will_mount = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"will-mount","will-mount",-434633071),new cljs.core.Keyword(null,"before-render","before-render",71256781)], null),mixins);
var render__$1 = render;
var wrap_render = rum.util.collect.call(null,new cljs.core.Keyword(null,"wrap-render","wrap-render",1782000986),mixins);
var wrapped_render = cljs.core.reduce.call(null,((function (init,will_mount,render__$1,wrap_render){
return (function (p1__11047_SHARP_,p2__11046_SHARP_){
return p2__11046_SHARP_.call(null,p1__11047_SHARP_);
});})(init,will_mount,render__$1,wrap_render))
,render__$1,wrap_render);
var did_mount = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"did-mount","did-mount",918232960),new cljs.core.Keyword(null,"after-render","after-render",1997533433)], null),mixins);
var did_remount = rum.util.collect.call(null,new cljs.core.Keyword(null,"did-remount","did-remount",1362550500),mixins);
var should_update = rum.util.collect.call(null,new cljs.core.Keyword(null,"should-update","should-update",-1292781795),mixins);
var will_update = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"will-update","will-update",328062998),new cljs.core.Keyword(null,"before-render","before-render",71256781)], null),mixins);
var did_update = rum.util.collect_STAR_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"did-update","did-update",-2143702256),new cljs.core.Keyword(null,"after-render","after-render",1997533433)], null),mixins);
var did_catch = rum.util.collect.call(null,new cljs.core.Keyword(null,"did-catch","did-catch",2139522313),mixins);
var will_unmount = rum.util.collect.call(null,new cljs.core.Keyword(null,"will-unmount","will-unmount",-808051550),mixins);
var child_context = rum.util.collect.call(null,new cljs.core.Keyword(null,"child-context","child-context",-1375270295),mixins);
var class_props = cljs.core.reduce.call(null,cljs.core.merge,rum.util.collect.call(null,new cljs.core.Keyword(null,"class-properties","class-properties",1351279702),mixins));
var static_props = cljs.core.reduce.call(null,cljs.core.merge,rum.util.collect.call(null,new cljs.core.Keyword(null,"static-properties","static-properties",-577838503),mixins));
var ctor = ((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props){
return (function (props){
var this$ = this;
goog.object.set(this$,"state",({":rum/state": cljs.core.volatile_BANG_.call(null,rum.util.call_all.call(null,cljs.core.assoc.call(null,goog.object.get(props,":rum/initial-state"),new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248),this$),init,props))}));

return React.Component.call(this$,props);
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props))
;
var _ = goog.inherits(ctor,React.Component);
var prototype = goog.object.get(ctor,"prototype");
if(cljs.core.empty_QMARK_.call(null,will_mount)){
} else {
goog.object.set(prototype,"componentWillMount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),will_mount));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,did_mount)){
} else {
goog.object.set(prototype,"componentDidMount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_mount));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"componentWillReceiveProps",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (next_props){
var this$ = this;
var old_state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
var state = cljs.core.merge.call(null,old_state,goog.object.get(next_props,":rum/initial-state"));
var next_state = cljs.core.reduce.call(null,((function (old_state,state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__11049_SHARP_,p2__11048_SHARP_){
return p2__11048_SHARP_.call(null,old_state,p1__11049_SHARP_);
});})(old_state,state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
,state,did_remount);
return this$.setState(({":rum/state": cljs.core.volatile_BANG_.call(null,next_state)}));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,should_update)){
} else {
goog.object.set(prototype,"shouldComponentUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (next_props,next_state){
var this$ = this;
var old_state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
var new_state = cljs.core.deref.call(null,goog.object.get(next_state,":rum/state"));
var or__4131__auto__ = cljs.core.some.call(null,((function (old_state,new_state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__11050_SHARP_){
return p1__11050_SHARP_.call(null,old_state,new_state);
});})(old_state,new_state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
,should_update);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return false;
}
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,will_update)){
} else {
goog.object.set(prototype,"componentWillUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (___$1,next_state){
var this$ = this;
var new_state = goog.object.get(next_state,":rum/state");
return cljs.core._vreset_BANG_.call(null,new_state,rum.util.call_all.call(null,cljs.core._deref.call(null,new_state),will_update));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"render",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
var state = rum.core.state.call(null,this$);
var vec__11052 = wrapped_render.call(null,cljs.core.deref.call(null,state));
var dom = cljs.core.nth.call(null,vec__11052,(0),null);
var next_state = cljs.core.nth.call(null,vec__11052,(1),null);
cljs.core.vreset_BANG_.call(null,state,next_state);

return dom;
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,did_update)){
} else {
goog.object.set(prototype,"componentDidUpdate",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (___$1,___$2){
var this$ = this;
return cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_update));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

if(cljs.core.empty_QMARK_.call(null,did_catch)){
} else {
goog.object.set(prototype,"componentDidCatch",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (error,info){
var this$ = this;
cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),did_catch,error,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","component-stack","rum/component-stack",2037541138),goog.object.get(info,"componentStack")], null)));

return this$.forceUpdate();
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

goog.object.set(prototype,"componentWillUnmount",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
if(cljs.core.empty_QMARK_.call(null,will_unmount)){
} else {
cljs.core._vreset_BANG_.call(null,rum.core.state.call(null,this$),rum.util.call_all.call(null,cljs.core._deref.call(null,rum.core.state.call(null,this$)),will_unmount));
}

return goog.object.set(this$,":rum/unmounted?",true);
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);

if(cljs.core.empty_QMARK_.call(null,child_context)){
} else {
goog.object.set(prototype,"getChildContext",((function (init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (){
var this$ = this;
var state = cljs.core.deref.call(null,rum.core.state.call(null,this$));
return cljs.core.clj__GT_js.call(null,cljs.core.transduce.call(null,cljs.core.map.call(null,((function (state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype){
return (function (p1__11051_SHARP_){
return p1__11051_SHARP_.call(null,state);
});})(state,this$,init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
),cljs.core.merge,cljs.core.PersistentArrayMap.EMPTY,child_context));
});})(init,will_mount,render__$1,wrap_render,wrapped_render,did_mount,did_remount,should_update,will_update,did_update,did_catch,will_unmount,child_context,class_props,static_props,ctor,_,prototype))
);
}

rum.core.extend_BANG_.call(null,prototype,class_props);

goog.object.set(ctor,"displayName",display_name);

rum.core.extend_BANG_.call(null,ctor,static_props);

return ctor;
});
rum.core.build_ctor = (function rum$core$build_ctor(render,mixins,display_name){
var class$ = rum.core.build_class.call(null,render,mixins,display_name);
var key_fn = cljs.core.first.call(null,rum.util.collect.call(null,new cljs.core.Keyword(null,"key-fn","key-fn",-636154479),mixins));
var ctor = (((!((key_fn == null))))?((function (class$,key_fn){
return (function() { 
var G__11055__delegate = function (args){
var props = ({":rum/initial-state": new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","args","rum/args",1315791754),args], null), "key": cljs.core.apply.call(null,key_fn,args)});
return React.createElement(class$,props);
};
var G__11055 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__11056__i = 0, G__11056__a = new Array(arguments.length -  0);
while (G__11056__i < G__11056__a.length) {G__11056__a[G__11056__i] = arguments[G__11056__i + 0]; ++G__11056__i;}
  args = new cljs.core.IndexedSeq(G__11056__a,0,null);
} 
return G__11055__delegate.call(this,args);};
G__11055.cljs$lang$maxFixedArity = 0;
G__11055.cljs$lang$applyTo = (function (arglist__11057){
var args = cljs.core.seq(arglist__11057);
return G__11055__delegate(args);
});
G__11055.cljs$core$IFn$_invoke$arity$variadic = G__11055__delegate;
return G__11055;
})()
;})(class$,key_fn))
:((function (class$,key_fn){
return (function() { 
var G__11058__delegate = function (args){
var props = ({":rum/initial-state": new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","args","rum/args",1315791754),args], null)});
return React.createElement(class$,props);
};
var G__11058 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__11059__i = 0, G__11059__a = new Array(arguments.length -  0);
while (G__11059__i < G__11059__a.length) {G__11059__a[G__11059__i] = arguments[G__11059__i + 0]; ++G__11059__i;}
  args = new cljs.core.IndexedSeq(G__11059__a,0,null);
} 
return G__11058__delegate.call(this,args);};
G__11058.cljs$lang$maxFixedArity = 0;
G__11058.cljs$lang$applyTo = (function (arglist__11060){
var args = cljs.core.seq(arglist__11060);
return G__11058__delegate(args);
});
G__11058.cljs$core$IFn$_invoke$arity$variadic = G__11058__delegate;
return G__11058;
})()
;})(class$,key_fn))
);
return cljs.core.with_meta.call(null,ctor,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","class","rum/class",-2030775258),class$], null));
});
rum.core.build_defc = (function rum$core$build_defc(render_body,mixins,display_name){
if(cljs.core.empty_QMARK_.call(null,mixins)){
var class$ = (function (props){
return cljs.core.apply.call(null,render_body,(props[":rum/args"]));
});
var _ = (class$["displayName"] = display_name);
var ctor = ((function (class$,_){
return (function() { 
var G__11061__delegate = function (args){
return React.createElement(class$,({":rum/args": args}));
};
var G__11061 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__11062__i = 0, G__11062__a = new Array(arguments.length -  0);
while (G__11062__i < G__11062__a.length) {G__11062__a[G__11062__i] = arguments[G__11062__i + 0]; ++G__11062__i;}
  args = new cljs.core.IndexedSeq(G__11062__a,0,null);
} 
return G__11061__delegate.call(this,args);};
G__11061.cljs$lang$maxFixedArity = 0;
G__11061.cljs$lang$applyTo = (function (arglist__11063){
var args = cljs.core.seq(arglist__11063);
return G__11061__delegate(args);
});
G__11061.cljs$core$IFn$_invoke$arity$variadic = G__11061__delegate;
return G__11061;
})()
;})(class$,_))
;
return cljs.core.with_meta.call(null,ctor,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("rum","class","rum/class",-2030775258),class$], null));
} else {
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
}
});
rum.core.build_defcs = (function rum$core$build_defcs(render_body,mixins,display_name){
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,state,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
});
rum.core.build_defcc = (function rum$core$build_defcc(render_body,mixins,display_name){
var render = (function (state){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.apply.call(null,render_body,new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state),new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(state)),state], null);
});
return rum.core.build_ctor.call(null,render,mixins,display_name);
});
rum.core.schedule = (function (){var or__4131__auto__ = (function (){var and__4120__auto__ = (typeof window !== 'undefined');
if(and__4120__auto__){
var or__4131__auto__ = window.requestAnimationFrame;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = window.webkitRequestAnimationFrame;
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
var or__4131__auto____$2 = window.mozRequestAnimationFrame;
if(cljs.core.truth_(or__4131__auto____$2)){
return or__4131__auto____$2;
} else {
return window.msRequestAnimationFrame;
}
}
}
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return ((function (or__4131__auto__){
return (function (p1__11064_SHARP_){
return setTimeout(p1__11064_SHARP_,(16));
});
;})(or__4131__auto__))
}
})();
rum.core.batch = (function (){var or__4131__auto__ = (((typeof ReactNative !== 'undefined'))?ReactNative.unstable_batchedUpdates:null);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var or__4131__auto____$1 = (((typeof ReactDOM !== 'undefined'))?ReactDOM.unstable_batchedUpdates:null);
if(cljs.core.truth_(or__4131__auto____$1)){
return or__4131__auto____$1;
} else {
return ((function (or__4131__auto____$1,or__4131__auto__){
return (function (f,a){
return f.call(null,a);
});
;})(or__4131__auto____$1,or__4131__auto__))
}
}
})();
rum.core.empty_queue = cljs.core.PersistentVector.EMPTY;
rum.core.render_queue = cljs.core.volatile_BANG_.call(null,rum.core.empty_queue);
rum.core.render_all = (function rum$core$render_all(queue){
var seq__11065 = cljs.core.seq.call(null,queue);
var chunk__11067 = null;
var count__11068 = (0);
var i__11069 = (0);
while(true){
if((i__11069 < count__11068)){
var comp = cljs.core._nth.call(null,chunk__11067,i__11069);
if(cljs.core.not.call(null,goog.object.get(comp,":rum/unmounted?"))){
comp.forceUpdate();


var G__11071 = seq__11065;
var G__11072 = chunk__11067;
var G__11073 = count__11068;
var G__11074 = (i__11069 + (1));
seq__11065 = G__11071;
chunk__11067 = G__11072;
count__11068 = G__11073;
i__11069 = G__11074;
continue;
} else {
var G__11075 = seq__11065;
var G__11076 = chunk__11067;
var G__11077 = count__11068;
var G__11078 = (i__11069 + (1));
seq__11065 = G__11075;
chunk__11067 = G__11076;
count__11068 = G__11077;
i__11069 = G__11078;
continue;
}
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__11065);
if(temp__5457__auto__){
var seq__11065__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11065__$1)){
var c__4550__auto__ = cljs.core.chunk_first.call(null,seq__11065__$1);
var G__11079 = cljs.core.chunk_rest.call(null,seq__11065__$1);
var G__11080 = c__4550__auto__;
var G__11081 = cljs.core.count.call(null,c__4550__auto__);
var G__11082 = (0);
seq__11065 = G__11079;
chunk__11067 = G__11080;
count__11068 = G__11081;
i__11069 = G__11082;
continue;
} else {
var comp = cljs.core.first.call(null,seq__11065__$1);
if(cljs.core.not.call(null,goog.object.get(comp,":rum/unmounted?"))){
comp.forceUpdate();


var G__11083 = cljs.core.next.call(null,seq__11065__$1);
var G__11084 = null;
var G__11085 = (0);
var G__11086 = (0);
seq__11065 = G__11083;
chunk__11067 = G__11084;
count__11068 = G__11085;
i__11069 = G__11086;
continue;
} else {
var G__11087 = cljs.core.next.call(null,seq__11065__$1);
var G__11088 = null;
var G__11089 = (0);
var G__11090 = (0);
seq__11065 = G__11087;
chunk__11067 = G__11088;
count__11068 = G__11089;
i__11069 = G__11090;
continue;
}
}
} else {
return null;
}
}
break;
}
});
rum.core.render = (function rum$core$render(){
var queue = cljs.core.deref.call(null,rum.core.render_queue);
cljs.core.vreset_BANG_.call(null,rum.core.render_queue,rum.core.empty_queue);

return rum.core.batch.call(null,rum.core.render_all,queue);
});
/**
 * Schedules react component to be rendered on next animation frame
 */
rum.core.request_render = (function rum$core$request_render(component){
if(cljs.core.empty_QMARK_.call(null,cljs.core.deref.call(null,rum.core.render_queue))){
rum.core.schedule.call(null,rum.core.render);
} else {
}

return cljs.core._vreset_BANG_.call(null,rum.core.render_queue,cljs.core.conj.call(null,cljs.core._deref.call(null,rum.core.render_queue),component));
});
/**
 * Add component to the DOM tree. Idempotent. Subsequent mounts will just update component
 */
rum.core.mount = (function rum$core$mount(component,node){
ReactDOM.render(component,node);

return null;
});
/**
 * Removes component from the DOM tree
 */
rum.core.unmount = (function rum$core$unmount(node){
return ReactDOM.unmountComponentAtNode(node);
});
/**
 * Hydrates server rendered DOM tree with provided component.
 */
rum.core.hydrate = (function rum$core$hydrate(component,node){
return ReactDOM.hydrate(component,node);
});
/**
 * Render `component` in a DOM `node` that might be ouside of current DOM hierarchy
 */
rum.core.portal = (function rum$core$portal(component,node){
return ReactDOM.createPortal(component,node);
});
/**
 * Adds React key to component
 */
rum.core.with_key = (function rum$core$with_key(component,key){
return React.cloneElement(component,({"key": key}),null);
});
/**
 * Adds React ref (string or callback) to component
 */
rum.core.with_ref = (function rum$core$with_ref(component,ref){
return React.cloneElement(component,({"ref": ref}),null);
});
/**
 * Given state, returns top-level DOM node. Can’t be called during render
 */
rum.core.dom_node = (function rum$core$dom_node(state){
return ReactDOM.findDOMNode(new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state));
});
/**
 * Given state and ref handle, returns React component
 */
rum.core.ref = (function rum$core$ref(state,key){
return ((new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state)["refs"])[cljs.core.name.call(null,key)]);
});
/**
 * Given state and ref handle, returns DOM node associated with ref
 */
rum.core.ref_node = (function rum$core$ref_node(state,key){
return ReactDOM.findDOMNode(rum.core.ref.call(null,state,cljs.core.name.call(null,key)));
});
/**
 * Mixin. Will avoid re-render if none of component’s arguments have changed.
 * Does equality check (=) on all arguments
 */
rum.core.static$ = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"should-update","should-update",-1292781795),(function (old_state,new_state){
return cljs.core.not_EQ_.call(null,new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(old_state),new cljs.core.Keyword("rum","args","rum/args",1315791754).cljs$core$IFn$_invoke$arity$1(new_state));
})], null);
/**
 * Mixin constructor. Adds an atom to component’s state that can be used to keep stuff
 * during component’s lifecycle. Component will be re-rendered if atom’s value changes.
 * Atom is stored under user-provided key or under `:rum/local` by default
 */
rum.core.local = (function rum$core$local(var_args){
var G__11092 = arguments.length;
switch (G__11092) {
case 1:
return rum.core.local.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return rum.core.local.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

rum.core.local.cljs$core$IFn$_invoke$arity$1 = (function (initial){
return rum.core.local.call(null,initial,new cljs.core.Keyword("rum","local","rum/local",-1497916586));
});

rum.core.local.cljs$core$IFn$_invoke$arity$2 = (function (initial,key){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"will-mount","will-mount",-434633071),(function (state){
var local_state = cljs.core.atom.call(null,initial);
var component = new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state);
cljs.core.add_watch.call(null,local_state,key,((function (local_state,component){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,component);
});})(local_state,component))
);

return cljs.core.assoc.call(null,state,key,local_state);
})], null);
});

rum.core.local.cljs$lang$maxFixedArity = 2;

/**
 * Mixin. Works in conjunction with `rum.core/react`
 */
rum.core.reactive = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"init","init",-1875481434),(function (state,props){
return cljs.core.assoc.call(null,state,new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142),cljs.core.random_uuid.call(null));
}),new cljs.core.Keyword(null,"wrap-render","wrap-render",1782000986),(function (render_fn){
return (function (state){
var _STAR_reactions_STAR__orig_val__11094 = rum.core._STAR_reactions_STAR_;
var _STAR_reactions_STAR__temp_val__11095 = cljs.core.volatile_BANG_.call(null,cljs.core.PersistentHashSet.EMPTY);
rum.core._STAR_reactions_STAR_ = _STAR_reactions_STAR__temp_val__11095;

try{var comp = new cljs.core.Keyword("rum","react-component","rum/react-component",-1879897248).cljs$core$IFn$_invoke$arity$1(state);
var old_reactions = new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325).cljs$core$IFn$_invoke$arity$2(state,cljs.core.PersistentHashSet.EMPTY);
var vec__11096 = render_fn.call(null,state);
var dom = cljs.core.nth.call(null,vec__11096,(0),null);
var next_state = cljs.core.nth.call(null,vec__11096,(1),null);
var new_reactions = cljs.core.deref.call(null,rum.core._STAR_reactions_STAR_);
var key = new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142).cljs$core$IFn$_invoke$arity$1(state);
var seq__11099_11111 = cljs.core.seq.call(null,old_reactions);
var chunk__11100_11112 = null;
var count__11101_11113 = (0);
var i__11102_11114 = (0);
while(true){
if((i__11102_11114 < count__11101_11113)){
var ref_11115 = cljs.core._nth.call(null,chunk__11100_11112,i__11102_11114);
if(cljs.core.contains_QMARK_.call(null,new_reactions,ref_11115)){
} else {
cljs.core.remove_watch.call(null,ref_11115,key);
}


var G__11116 = seq__11099_11111;
var G__11117 = chunk__11100_11112;
var G__11118 = count__11101_11113;
var G__11119 = (i__11102_11114 + (1));
seq__11099_11111 = G__11116;
chunk__11100_11112 = G__11117;
count__11101_11113 = G__11118;
i__11102_11114 = G__11119;
continue;
} else {
var temp__5457__auto___11120 = cljs.core.seq.call(null,seq__11099_11111);
if(temp__5457__auto___11120){
var seq__11099_11121__$1 = temp__5457__auto___11120;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11099_11121__$1)){
var c__4550__auto___11122 = cljs.core.chunk_first.call(null,seq__11099_11121__$1);
var G__11123 = cljs.core.chunk_rest.call(null,seq__11099_11121__$1);
var G__11124 = c__4550__auto___11122;
var G__11125 = cljs.core.count.call(null,c__4550__auto___11122);
var G__11126 = (0);
seq__11099_11111 = G__11123;
chunk__11100_11112 = G__11124;
count__11101_11113 = G__11125;
i__11102_11114 = G__11126;
continue;
} else {
var ref_11127 = cljs.core.first.call(null,seq__11099_11121__$1);
if(cljs.core.contains_QMARK_.call(null,new_reactions,ref_11127)){
} else {
cljs.core.remove_watch.call(null,ref_11127,key);
}


var G__11128 = cljs.core.next.call(null,seq__11099_11121__$1);
var G__11129 = null;
var G__11130 = (0);
var G__11131 = (0);
seq__11099_11111 = G__11128;
chunk__11100_11112 = G__11129;
count__11101_11113 = G__11130;
i__11102_11114 = G__11131;
continue;
}
} else {
}
}
break;
}

var seq__11103_11132 = cljs.core.seq.call(null,new_reactions);
var chunk__11104_11133 = null;
var count__11105_11134 = (0);
var i__11106_11135 = (0);
while(true){
if((i__11106_11135 < count__11105_11134)){
var ref_11136 = cljs.core._nth.call(null,chunk__11104_11133,i__11106_11135);
if(cljs.core.contains_QMARK_.call(null,old_reactions,ref_11136)){
} else {
cljs.core.add_watch.call(null,ref_11136,key,((function (seq__11103_11132,chunk__11104_11133,count__11105_11134,i__11106_11135,ref_11136,comp,old_reactions,vec__11096,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__11094,_STAR_reactions_STAR__temp_val__11095){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,comp);
});})(seq__11103_11132,chunk__11104_11133,count__11105_11134,i__11106_11135,ref_11136,comp,old_reactions,vec__11096,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__11094,_STAR_reactions_STAR__temp_val__11095))
);
}


var G__11137 = seq__11103_11132;
var G__11138 = chunk__11104_11133;
var G__11139 = count__11105_11134;
var G__11140 = (i__11106_11135 + (1));
seq__11103_11132 = G__11137;
chunk__11104_11133 = G__11138;
count__11105_11134 = G__11139;
i__11106_11135 = G__11140;
continue;
} else {
var temp__5457__auto___11141 = cljs.core.seq.call(null,seq__11103_11132);
if(temp__5457__auto___11141){
var seq__11103_11142__$1 = temp__5457__auto___11141;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11103_11142__$1)){
var c__4550__auto___11143 = cljs.core.chunk_first.call(null,seq__11103_11142__$1);
var G__11144 = cljs.core.chunk_rest.call(null,seq__11103_11142__$1);
var G__11145 = c__4550__auto___11143;
var G__11146 = cljs.core.count.call(null,c__4550__auto___11143);
var G__11147 = (0);
seq__11103_11132 = G__11144;
chunk__11104_11133 = G__11145;
count__11105_11134 = G__11146;
i__11106_11135 = G__11147;
continue;
} else {
var ref_11148 = cljs.core.first.call(null,seq__11103_11142__$1);
if(cljs.core.contains_QMARK_.call(null,old_reactions,ref_11148)){
} else {
cljs.core.add_watch.call(null,ref_11148,key,((function (seq__11103_11132,chunk__11104_11133,count__11105_11134,i__11106_11135,ref_11148,seq__11103_11142__$1,temp__5457__auto___11141,comp,old_reactions,vec__11096,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__11094,_STAR_reactions_STAR__temp_val__11095){
return (function (_,___$1,___$2,___$3){
return rum.core.request_render.call(null,comp);
});})(seq__11103_11132,chunk__11104_11133,count__11105_11134,i__11106_11135,ref_11148,seq__11103_11142__$1,temp__5457__auto___11141,comp,old_reactions,vec__11096,dom,next_state,new_reactions,key,_STAR_reactions_STAR__orig_val__11094,_STAR_reactions_STAR__temp_val__11095))
);
}


var G__11149 = cljs.core.next.call(null,seq__11103_11142__$1);
var G__11150 = null;
var G__11151 = (0);
var G__11152 = (0);
seq__11103_11132 = G__11149;
chunk__11104_11133 = G__11150;
count__11105_11134 = G__11151;
i__11106_11135 = G__11152;
continue;
}
} else {
}
}
break;
}

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [dom,cljs.core.assoc.call(null,next_state,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325),new_reactions)], null);
}finally {rum.core._STAR_reactions_STAR_ = _STAR_reactions_STAR__orig_val__11094;
}});
}),new cljs.core.Keyword(null,"will-unmount","will-unmount",-808051550),(function (state){
var key_11153 = new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142).cljs$core$IFn$_invoke$arity$1(state);
var seq__11107_11154 = cljs.core.seq.call(null,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325).cljs$core$IFn$_invoke$arity$1(state));
var chunk__11108_11155 = null;
var count__11109_11156 = (0);
var i__11110_11157 = (0);
while(true){
if((i__11110_11157 < count__11109_11156)){
var ref_11158 = cljs.core._nth.call(null,chunk__11108_11155,i__11110_11157);
cljs.core.remove_watch.call(null,ref_11158,key_11153);


var G__11159 = seq__11107_11154;
var G__11160 = chunk__11108_11155;
var G__11161 = count__11109_11156;
var G__11162 = (i__11110_11157 + (1));
seq__11107_11154 = G__11159;
chunk__11108_11155 = G__11160;
count__11109_11156 = G__11161;
i__11110_11157 = G__11162;
continue;
} else {
var temp__5457__auto___11163 = cljs.core.seq.call(null,seq__11107_11154);
if(temp__5457__auto___11163){
var seq__11107_11164__$1 = temp__5457__auto___11163;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11107_11164__$1)){
var c__4550__auto___11165 = cljs.core.chunk_first.call(null,seq__11107_11164__$1);
var G__11166 = cljs.core.chunk_rest.call(null,seq__11107_11164__$1);
var G__11167 = c__4550__auto___11165;
var G__11168 = cljs.core.count.call(null,c__4550__auto___11165);
var G__11169 = (0);
seq__11107_11154 = G__11166;
chunk__11108_11155 = G__11167;
count__11109_11156 = G__11168;
i__11110_11157 = G__11169;
continue;
} else {
var ref_11170 = cljs.core.first.call(null,seq__11107_11164__$1);
cljs.core.remove_watch.call(null,ref_11170,key_11153);


var G__11171 = cljs.core.next.call(null,seq__11107_11164__$1);
var G__11172 = null;
var G__11173 = (0);
var G__11174 = (0);
seq__11107_11154 = G__11171;
chunk__11108_11155 = G__11172;
count__11109_11156 = G__11173;
i__11110_11157 = G__11174;
continue;
}
} else {
}
}
break;
}

return cljs.core.dissoc.call(null,state,new cljs.core.Keyword("rum.reactive","refs","rum.reactive/refs",-814076325),new cljs.core.Keyword("rum.reactive","key","rum.reactive/key",-803425142));
})], null);
/**
 * Works in conjunction with `rum.core/reactive` mixin. Use this function instead of
 * `deref` inside render, and your component will subscribe to changes happening
 * to the derefed atom.
 */
rum.core.react = (function rum$core$react(ref){
if(cljs.core.truth_(rum.core._STAR_reactions_STAR_)){
} else {
throw (new Error(["Assert failed: ","rum.core/react is only supported in conjunction with rum.core/reactive","\n","*reactions*"].join('')));
}

cljs.core._vreset_BANG_.call(null,rum.core._STAR_reactions_STAR_,cljs.core.conj.call(null,cljs.core._deref.call(null,rum.core._STAR_reactions_STAR_),ref));

return cljs.core.deref.call(null,ref);
});
/**
 * Use this to create “chains” and acyclic graphs of dependent atoms.
 * `derived-atom` will:
 *  - Take N “source” refs
 *  - Set up a watch on each of them
 *  - Create “sink” atom
 *  - When any of source refs changes:
 *     - re-run function `f`, passing N dereferenced values of source refs
 *     - `reset!` result of `f` to the sink atom
 *  - return sink atom
 * 
 *  (def *a (atom 0))
 *  (def *b (atom 1))
 *  (def *x (derived-atom [*a *b] ::key
 *            (fn [a b]
 *              (str a ":" b))))
 *  (type *x) ;; => clojure.lang.Atom
 *  \@*x     ;; => 0:1
 *  (swap! *a inc)
 *  \@*x     ;; => 1:1
 *  (reset! *b 7)
 *  \@*x     ;; => 1:7
 * 
 * Arguments:
 *   refs - sequence of source refs
 *   key  - unique key to register watcher, see `clojure.core/add-watch`
 *   f    - function that must accept N arguments (same as number of source refs)
 *          and return a value to be written to the sink ref.
 *          Note: `f` will be called with already dereferenced values
 *   opts - optional. Map of:
 *     :ref           - Use this as sink ref. By default creates new atom
 *     :check-equals? - Do an equality check on each update: `(= @sink (f new-vals))`.
 *                      If result of `f` is equal to the old one, do not call `reset!`.
 *                      Defaults to `true`. Set to false if calling `=` would be expensive
 */
rum.core.derived_atom = rum.derived_atom.derived_atom;
/**
 * Given atom with deep nested value and path inside it, creates an atom-like structure
 * that can be used separately from main atom, but will sync changes both ways:
 *   
 *   (def db (atom { :users { "Ivan" { :age 30 }}}))
 *   (def ivan (rum/cursor db [:users "Ivan"]))
 *   \@ivan ;; => { :age 30 }
 *   (swap! ivan update :age inc) ;; => { :age 31 }
 *   \@db ;; => { :users { "Ivan" { :age 31 }}}
 *   (swap! db update-in [:users "Ivan" :age] inc) ;; => { :users { "Ivan" { :age 32 }}}
 *   \@ivan ;; => { :age 32 }
 *   
 *   Returned value supports deref, swap!, reset!, watches and metadata.
 *   The only supported option is `:meta`
 */
rum.core.cursor_in = (function rum$core$cursor_in(var_args){
var args__4736__auto__ = [];
var len__4730__auto___11181 = arguments.length;
var i__4731__auto___11182 = (0);
while(true){
if((i__4731__auto___11182 < len__4730__auto___11181)){
args__4736__auto__.push((arguments[i__4731__auto___11182]));

var G__11183 = (i__4731__auto___11182 + (1));
i__4731__auto___11182 = G__11183;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((2) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((2)),(0),null)):null);
return rum.core.cursor_in.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4737__auto__);
});

rum.core.cursor_in.cljs$core$IFn$_invoke$arity$variadic = (function (ref,path,p__11178){
var map__11179 = p__11178;
var map__11179__$1 = (((((!((map__11179 == null))))?(((((map__11179.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__11179.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__11179):map__11179);
var options = map__11179__$1;
if((ref instanceof rum.cursor.Cursor)){
return (new rum.cursor.Cursor(ref.ref,cljs.core.into.call(null,ref.path,path),new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(options)));
} else {
return (new rum.cursor.Cursor(ref,path,new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(options)));
}
});

rum.core.cursor_in.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
rum.core.cursor_in.cljs$lang$applyTo = (function (seq11175){
var G__11176 = cljs.core.first.call(null,seq11175);
var seq11175__$1 = cljs.core.next.call(null,seq11175);
var G__11177 = cljs.core.first.call(null,seq11175__$1);
var seq11175__$2 = cljs.core.next.call(null,seq11175__$1);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__11176,G__11177,seq11175__$2);
});

/**
 * Same as `rum.core/cursor-in` but accepts single key instead of path vector
 */
rum.core.cursor = (function rum$core$cursor(var_args){
var args__4736__auto__ = [];
var len__4730__auto___11187 = arguments.length;
var i__4731__auto___11188 = (0);
while(true){
if((i__4731__auto___11188 < len__4730__auto___11187)){
args__4736__auto__.push((arguments[i__4731__auto___11188]));

var G__11189 = (i__4731__auto___11188 + (1));
i__4731__auto___11188 = G__11189;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((2) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((2)),(0),null)):null);
return rum.core.cursor.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4737__auto__);
});

rum.core.cursor.cljs$core$IFn$_invoke$arity$variadic = (function (ref,key,options){
return cljs.core.apply.call(null,rum.core.cursor_in,ref,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [key], null),options);
});

rum.core.cursor.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
rum.core.cursor.cljs$lang$applyTo = (function (seq11184){
var G__11185 = cljs.core.first.call(null,seq11184);
var seq11184__$1 = cljs.core.next.call(null,seq11184);
var G__11186 = cljs.core.first.call(null,seq11184__$1);
var seq11184__$2 = cljs.core.next.call(null,seq11184__$1);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__11185,G__11186,seq11184__$2);
});


//# sourceMappingURL=core.js.map
