// Compiled by ClojureScript 1.10.520 {}
goog.provide('sablono.core');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('goog.string');
goog.require('sablono.normalize');
goog.require('sablono.util');
goog.require('sablono.interpreter');
goog.require('goog.dom');
/**
 * Add an optional attribute argument to a function that returns a element vector.
 */
sablono.core.wrap_attrs = (function sablono$core$wrap_attrs(func){
return (function() { 
var G__10440__delegate = function (args){
if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,args))){
var vec__10437 = cljs.core.apply.call(null,func,cljs.core.rest.call(null,args));
var seq__10438 = cljs.core.seq.call(null,vec__10437);
var first__10439 = cljs.core.first.call(null,seq__10438);
var seq__10438__$1 = cljs.core.next.call(null,seq__10438);
var tag = first__10439;
var body = seq__10438__$1;
if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,body))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.merge.call(null,cljs.core.first.call(null,body),cljs.core.first.call(null,args))], null),cljs.core.rest.call(null,body));
} else {
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.first.call(null,args)], null),body);
}
} else {
return cljs.core.apply.call(null,func,args);
}
};
var G__10440 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__10441__i = 0, G__10441__a = new Array(arguments.length -  0);
while (G__10441__i < G__10441__a.length) {G__10441__a[G__10441__i] = arguments[G__10441__i + 0]; ++G__10441__i;}
  args = new cljs.core.IndexedSeq(G__10441__a,0,null);
} 
return G__10440__delegate.call(this,args);};
G__10440.cljs$lang$maxFixedArity = 0;
G__10440.cljs$lang$applyTo = (function (arglist__10442){
var args = cljs.core.seq(arglist__10442);
return G__10440__delegate(args);
});
G__10440.cljs$core$IFn$_invoke$arity$variadic = G__10440__delegate;
return G__10440;
})()
;
});
sablono.core.update_arglists = (function sablono$core$update_arglists(arglists){
var iter__4523__auto__ = (function sablono$core$update_arglists_$_iter__10443(s__10444){
return (new cljs.core.LazySeq(null,(function (){
var s__10444__$1 = s__10444;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__10444__$1);
if(temp__5457__auto__){
var s__10444__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10444__$2)){
var c__4521__auto__ = cljs.core.chunk_first.call(null,s__10444__$2);
var size__4522__auto__ = cljs.core.count.call(null,c__4521__auto__);
var b__10446 = cljs.core.chunk_buffer.call(null,size__4522__auto__);
if((function (){var i__10445 = (0);
while(true){
if((i__10445 < size__4522__auto__)){
var args = cljs.core._nth.call(null,c__4521__auto__,i__10445);
cljs.core.chunk_append.call(null,b__10446,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)));

var G__10447 = (i__10445 + (1));
i__10445 = G__10447;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10446),sablono$core$update_arglists_$_iter__10443.call(null,cljs.core.chunk_rest.call(null,s__10444__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10446),null);
}
} else {
var args = cljs.core.first.call(null,s__10444__$2);
return cljs.core.cons.call(null,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)),sablono$core$update_arglists_$_iter__10443.call(null,cljs.core.rest.call(null,s__10444__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__.call(null,arglists);
});
/**
 * Include a list of external stylesheet files.
 */
sablono.core.include_css = (function sablono$core$include_css(var_args){
var args__4736__auto__ = [];
var len__4730__auto___10453 = arguments.length;
var i__4731__auto___10454 = (0);
while(true){
if((i__4731__auto___10454 < len__4730__auto___10453)){
args__4736__auto__.push((arguments[i__4731__auto___10454]));

var G__10455 = (i__4731__auto___10454 + (1));
i__4731__auto___10454 = G__10455;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic = (function (styles){
var iter__4523__auto__ = (function sablono$core$iter__10449(s__10450){
return (new cljs.core.LazySeq(null,(function (){
var s__10450__$1 = s__10450;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__10450__$1);
if(temp__5457__auto__){
var s__10450__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10450__$2)){
var c__4521__auto__ = cljs.core.chunk_first.call(null,s__10450__$2);
var size__4522__auto__ = cljs.core.count.call(null,c__4521__auto__);
var b__10452 = cljs.core.chunk_buffer.call(null,size__4522__auto__);
if((function (){var i__10451 = (0);
while(true){
if((i__10451 < size__4522__auto__)){
var style = cljs.core._nth.call(null,c__4521__auto__,i__10451);
cljs.core.chunk_append.call(null,b__10452,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null));

var G__10456 = (i__10451 + (1));
i__10451 = G__10456;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10452),sablono$core$iter__10449.call(null,cljs.core.chunk_rest.call(null,s__10450__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10452),null);
}
} else {
var style = cljs.core.first.call(null,s__10450__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null),sablono$core$iter__10449.call(null,cljs.core.rest.call(null,s__10450__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__.call(null,styles);
});

sablono.core.include_css.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
sablono.core.include_css.cljs$lang$applyTo = (function (seq10448){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq10448));
});

/**
 * Include the JavaScript library at `src`.
 */
sablono.core.include_js = (function sablono$core$include_js(src){
return goog.dom.appendChild(goog.dom.getDocument().body,goog.dom.createDom("script",({"src": src})));
});
/**
 * Include Facebook's React JavaScript library.
 */
sablono.core.include_react = (function sablono$core$include_react(){
return sablono.core.include_js.call(null,"http://fb.me/react-0.12.2.js");
});
/**
 * Wraps some content in a HTML hyperlink with the supplied URL.
 */
sablono.core.link_to10457 = (function sablono$core$link_to10457(var_args){
var args__4736__auto__ = [];
var len__4730__auto___10460 = arguments.length;
var i__4731__auto___10461 = (0);
while(true){
if((i__4731__auto___10461 < len__4730__auto___10460)){
args__4736__auto__.push((arguments[i__4731__auto___10461]));

var G__10462 = (i__4731__auto___10461 + (1));
i__4731__auto___10461 = G__10462;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return sablono.core.link_to10457.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

sablono.core.link_to10457.cljs$core$IFn$_invoke$arity$variadic = (function (url,content){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,url)], null),content], null);
});

sablono.core.link_to10457.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.link_to10457.cljs$lang$applyTo = (function (seq10458){
var G__10459 = cljs.core.first.call(null,seq10458);
var seq10458__$1 = cljs.core.next.call(null,seq10458);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__10459,seq10458__$1);
});


sablono.core.link_to = sablono.core.wrap_attrs.call(null,sablono.core.link_to10457);
/**
 * Wraps some content in a HTML hyperlink with the supplied e-mail
 *   address. If no content provided use the e-mail address as content.
 */
sablono.core.mail_to10463 = (function sablono$core$mail_to10463(var_args){
var args__4736__auto__ = [];
var len__4730__auto___10470 = arguments.length;
var i__4731__auto___10471 = (0);
while(true){
if((i__4731__auto___10471 < len__4730__auto___10470)){
args__4736__auto__.push((arguments[i__4731__auto___10471]));

var G__10472 = (i__4731__auto___10471 + (1));
i__4731__auto___10471 = G__10472;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return sablono.core.mail_to10463.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

sablono.core.mail_to10463.cljs$core$IFn$_invoke$arity$variadic = (function (e_mail,p__10466){
var vec__10467 = p__10466;
var content = cljs.core.nth.call(null,vec__10467,(0),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),["mailto:",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_mail)].join('')], null),(function (){var or__4131__auto__ = content;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return e_mail;
}
})()], null);
});

sablono.core.mail_to10463.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.mail_to10463.cljs$lang$applyTo = (function (seq10464){
var G__10465 = cljs.core.first.call(null,seq10464);
var seq10464__$1 = cljs.core.next.call(null,seq10464);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__10465,seq10464__$1);
});


sablono.core.mail_to = sablono.core.wrap_attrs.call(null,sablono.core.mail_to10463);
/**
 * Wrap a collection in an unordered list.
 */
sablono.core.unordered_list10473 = (function sablono$core$unordered_list10473(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4523__auto__ = (function sablono$core$unordered_list10473_$_iter__10474(s__10475){
return (new cljs.core.LazySeq(null,(function (){
var s__10475__$1 = s__10475;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__10475__$1);
if(temp__5457__auto__){
var s__10475__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10475__$2)){
var c__4521__auto__ = cljs.core.chunk_first.call(null,s__10475__$2);
var size__4522__auto__ = cljs.core.count.call(null,c__4521__auto__);
var b__10477 = cljs.core.chunk_buffer.call(null,size__4522__auto__);
if((function (){var i__10476 = (0);
while(true){
if((i__10476 < size__4522__auto__)){
var x = cljs.core._nth.call(null,c__4521__auto__,i__10476);
cljs.core.chunk_append.call(null,b__10477,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__10478 = (i__10476 + (1));
i__10476 = G__10478;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10477),sablono$core$unordered_list10473_$_iter__10474.call(null,cljs.core.chunk_rest.call(null,s__10475__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10477),null);
}
} else {
var x = cljs.core.first.call(null,s__10475__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$unordered_list10473_$_iter__10474.call(null,cljs.core.rest.call(null,s__10475__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__.call(null,coll);
})()], null);
});

sablono.core.unordered_list = sablono.core.wrap_attrs.call(null,sablono.core.unordered_list10473);
/**
 * Wrap a collection in an ordered list.
 */
sablono.core.ordered_list10479 = (function sablono$core$ordered_list10479(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),(function (){var iter__4523__auto__ = (function sablono$core$ordered_list10479_$_iter__10480(s__10481){
return (new cljs.core.LazySeq(null,(function (){
var s__10481__$1 = s__10481;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__10481__$1);
if(temp__5457__auto__){
var s__10481__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10481__$2)){
var c__4521__auto__ = cljs.core.chunk_first.call(null,s__10481__$2);
var size__4522__auto__ = cljs.core.count.call(null,c__4521__auto__);
var b__10483 = cljs.core.chunk_buffer.call(null,size__4522__auto__);
if((function (){var i__10482 = (0);
while(true){
if((i__10482 < size__4522__auto__)){
var x = cljs.core._nth.call(null,c__4521__auto__,i__10482);
cljs.core.chunk_append.call(null,b__10483,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__10484 = (i__10482 + (1));
i__10482 = G__10484;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10483),sablono$core$ordered_list10479_$_iter__10480.call(null,cljs.core.chunk_rest.call(null,s__10481__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10483),null);
}
} else {
var x = cljs.core.first.call(null,s__10481__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$ordered_list10479_$_iter__10480.call(null,cljs.core.rest.call(null,s__10481__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__.call(null,coll);
})()], null);
});

sablono.core.ordered_list = sablono.core.wrap_attrs.call(null,sablono.core.ordered_list10479);
/**
 * Create an image element.
 */
sablono.core.image10485 = (function sablono$core$image10485(var_args){
var G__10487 = arguments.length;
switch (G__10487) {
case 1:
return sablono.core.image10485.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.image10485.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.image10485.cljs$core$IFn$_invoke$arity$1 = (function (src){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.call(null,src)], null)], null);
});

sablono.core.image10485.cljs$core$IFn$_invoke$arity$2 = (function (src,alt){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.call(null,src),new cljs.core.Keyword(null,"alt","alt",-3214426),alt], null)], null);
});

sablono.core.image10485.cljs$lang$maxFixedArity = 2;


sablono.core.image = sablono.core.wrap_attrs.call(null,sablono.core.image10485);
sablono.core._STAR_group_STAR_ = cljs.core.PersistentVector.EMPTY;
/**
 * Create a field name from the supplied argument the current field group.
 */
sablono.core.make_name = (function sablono$core$make_name(name){
return cljs.core.reduce.call(null,(function (p1__10489_SHARP_,p2__10490_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__10489_SHARP_),"[",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__10490_SHARP_),"]"].join('');
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
 * Create a field id from the supplied argument and current field group.
 */
sablono.core.make_id = (function sablono$core$make_id(name){
return cljs.core.reduce.call(null,(function (p1__10491_SHARP_,p2__10492_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__10491_SHARP_),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__10492_SHARP_)].join('');
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
 * Creates a new <input> element.
 */
sablono.core.input_field_STAR_ = (function sablono$core$input_field_STAR_(var_args){
var G__10494 = arguments.length;
switch (G__10494) {
case 2:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (type,name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3 = (function (type,name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4131__auto__ = value;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return undefined;
}
})()], null)], null);
});

sablono.core.input_field_STAR_.cljs$lang$maxFixedArity = 3;

/**
 * Creates a color input field.
 */
sablono.core.color_field10496 = (function sablono$core$color_field10496(var_args){
var G__10498 = arguments.length;
switch (G__10498) {
case 1:
return sablono.core.color_field10496.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.color_field10496.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.color_field10496.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null)),name__10427__auto__);
});

sablono.core.color_field10496.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.color_field10496.cljs$lang$maxFixedArity = 2;


sablono.core.color_field = sablono.core.wrap_attrs.call(null,sablono.core.color_field10496);

/**
 * Creates a date input field.
 */
sablono.core.date_field10499 = (function sablono$core$date_field10499(var_args){
var G__10501 = arguments.length;
switch (G__10501) {
case 1:
return sablono.core.date_field10499.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.date_field10499.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.date_field10499.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null)),name__10427__auto__);
});

sablono.core.date_field10499.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.date_field10499.cljs$lang$maxFixedArity = 2;


sablono.core.date_field = sablono.core.wrap_attrs.call(null,sablono.core.date_field10499);

/**
 * Creates a datetime input field.
 */
sablono.core.datetime_field10502 = (function sablono$core$datetime_field10502(var_args){
var G__10504 = arguments.length;
switch (G__10504) {
case 1:
return sablono.core.datetime_field10502.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_field10502.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.datetime_field10502.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null)),name__10427__auto__);
});

sablono.core.datetime_field10502.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.datetime_field10502.cljs$lang$maxFixedArity = 2;


sablono.core.datetime_field = sablono.core.wrap_attrs.call(null,sablono.core.datetime_field10502);

/**
 * Creates a datetime-local input field.
 */
sablono.core.datetime_local_field10505 = (function sablono$core$datetime_local_field10505(var_args){
var G__10507 = arguments.length;
switch (G__10507) {
case 1:
return sablono.core.datetime_local_field10505.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_local_field10505.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.datetime_local_field10505.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null)),name__10427__auto__);
});

sablono.core.datetime_local_field10505.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.datetime_local_field10505.cljs$lang$maxFixedArity = 2;


sablono.core.datetime_local_field = sablono.core.wrap_attrs.call(null,sablono.core.datetime_local_field10505);

/**
 * Creates a email input field.
 */
sablono.core.email_field10508 = (function sablono$core$email_field10508(var_args){
var G__10510 = arguments.length;
switch (G__10510) {
case 1:
return sablono.core.email_field10508.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.email_field10508.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.email_field10508.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null)),name__10427__auto__);
});

sablono.core.email_field10508.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.email_field10508.cljs$lang$maxFixedArity = 2;


sablono.core.email_field = sablono.core.wrap_attrs.call(null,sablono.core.email_field10508);

/**
 * Creates a file input field.
 */
sablono.core.file_field10511 = (function sablono$core$file_field10511(var_args){
var G__10513 = arguments.length;
switch (G__10513) {
case 1:
return sablono.core.file_field10511.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.file_field10511.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.file_field10511.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null)),name__10427__auto__);
});

sablono.core.file_field10511.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.file_field10511.cljs$lang$maxFixedArity = 2;


sablono.core.file_field = sablono.core.wrap_attrs.call(null,sablono.core.file_field10511);

/**
 * Creates a hidden input field.
 */
sablono.core.hidden_field10514 = (function sablono$core$hidden_field10514(var_args){
var G__10516 = arguments.length;
switch (G__10516) {
case 1:
return sablono.core.hidden_field10514.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.hidden_field10514.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.hidden_field10514.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null)),name__10427__auto__);
});

sablono.core.hidden_field10514.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.hidden_field10514.cljs$lang$maxFixedArity = 2;


sablono.core.hidden_field = sablono.core.wrap_attrs.call(null,sablono.core.hidden_field10514);

/**
 * Creates a month input field.
 */
sablono.core.month_field10517 = (function sablono$core$month_field10517(var_args){
var G__10519 = arguments.length;
switch (G__10519) {
case 1:
return sablono.core.month_field10517.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.month_field10517.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.month_field10517.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null)),name__10427__auto__);
});

sablono.core.month_field10517.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.month_field10517.cljs$lang$maxFixedArity = 2;


sablono.core.month_field = sablono.core.wrap_attrs.call(null,sablono.core.month_field10517);

/**
 * Creates a number input field.
 */
sablono.core.number_field10520 = (function sablono$core$number_field10520(var_args){
var G__10522 = arguments.length;
switch (G__10522) {
case 1:
return sablono.core.number_field10520.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.number_field10520.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.number_field10520.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null)),name__10427__auto__);
});

sablono.core.number_field10520.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.number_field10520.cljs$lang$maxFixedArity = 2;


sablono.core.number_field = sablono.core.wrap_attrs.call(null,sablono.core.number_field10520);

/**
 * Creates a password input field.
 */
sablono.core.password_field10523 = (function sablono$core$password_field10523(var_args){
var G__10525 = arguments.length;
switch (G__10525) {
case 1:
return sablono.core.password_field10523.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.password_field10523.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.password_field10523.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null)),name__10427__auto__);
});

sablono.core.password_field10523.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.password_field10523.cljs$lang$maxFixedArity = 2;


sablono.core.password_field = sablono.core.wrap_attrs.call(null,sablono.core.password_field10523);

/**
 * Creates a range input field.
 */
sablono.core.range_field10526 = (function sablono$core$range_field10526(var_args){
var G__10528 = arguments.length;
switch (G__10528) {
case 1:
return sablono.core.range_field10526.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.range_field10526.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.range_field10526.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null)),name__10427__auto__);
});

sablono.core.range_field10526.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.range_field10526.cljs$lang$maxFixedArity = 2;


sablono.core.range_field = sablono.core.wrap_attrs.call(null,sablono.core.range_field10526);

/**
 * Creates a search input field.
 */
sablono.core.search_field10529 = (function sablono$core$search_field10529(var_args){
var G__10531 = arguments.length;
switch (G__10531) {
case 1:
return sablono.core.search_field10529.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.search_field10529.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.search_field10529.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null)),name__10427__auto__);
});

sablono.core.search_field10529.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.search_field10529.cljs$lang$maxFixedArity = 2;


sablono.core.search_field = sablono.core.wrap_attrs.call(null,sablono.core.search_field10529);

/**
 * Creates a tel input field.
 */
sablono.core.tel_field10532 = (function sablono$core$tel_field10532(var_args){
var G__10534 = arguments.length;
switch (G__10534) {
case 1:
return sablono.core.tel_field10532.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.tel_field10532.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.tel_field10532.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null)),name__10427__auto__);
});

sablono.core.tel_field10532.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.tel_field10532.cljs$lang$maxFixedArity = 2;


sablono.core.tel_field = sablono.core.wrap_attrs.call(null,sablono.core.tel_field10532);

/**
 * Creates a text input field.
 */
sablono.core.text_field10535 = (function sablono$core$text_field10535(var_args){
var G__10537 = arguments.length;
switch (G__10537) {
case 1:
return sablono.core.text_field10535.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_field10535.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.text_field10535.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null)),name__10427__auto__);
});

sablono.core.text_field10535.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.text_field10535.cljs$lang$maxFixedArity = 2;


sablono.core.text_field = sablono.core.wrap_attrs.call(null,sablono.core.text_field10535);

/**
 * Creates a time input field.
 */
sablono.core.time_field10538 = (function sablono$core$time_field10538(var_args){
var G__10540 = arguments.length;
switch (G__10540) {
case 1:
return sablono.core.time_field10538.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.time_field10538.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.time_field10538.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null)),name__10427__auto__);
});

sablono.core.time_field10538.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.time_field10538.cljs$lang$maxFixedArity = 2;


sablono.core.time_field = sablono.core.wrap_attrs.call(null,sablono.core.time_field10538);

/**
 * Creates a url input field.
 */
sablono.core.url_field10541 = (function sablono$core$url_field10541(var_args){
var G__10543 = arguments.length;
switch (G__10543) {
case 1:
return sablono.core.url_field10541.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.url_field10541.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.url_field10541.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null)),name__10427__auto__);
});

sablono.core.url_field10541.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.url_field10541.cljs$lang$maxFixedArity = 2;


sablono.core.url_field = sablono.core.wrap_attrs.call(null,sablono.core.url_field10541);

/**
 * Creates a week input field.
 */
sablono.core.week_field10544 = (function sablono$core$week_field10544(var_args){
var G__10546 = arguments.length;
switch (G__10546) {
case 1:
return sablono.core.week_field10544.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.week_field10544.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.week_field10544.cljs$core$IFn$_invoke$arity$1 = (function (name__10427__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null)),name__10427__auto__);
});

sablono.core.week_field10544.cljs$core$IFn$_invoke$arity$2 = (function (name__10427__auto__,value__10428__auto__){
return sablono.core.input_field_STAR_.call(null,cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null)),name__10427__auto__,value__10428__auto__);
});

sablono.core.week_field10544.cljs$lang$maxFixedArity = 2;


sablono.core.week_field = sablono.core.wrap_attrs.call(null,sablono.core.week_field10544);
sablono.core.file_upload = sablono.core.file_field;
/**
 * Creates a check box.
 */
sablono.core.check_box10564 = (function sablono$core$check_box10564(var_args){
var G__10566 = arguments.length;
switch (G__10566) {
case 1:
return sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$2 = (function (name,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.check_box10564.cljs$core$IFn$_invoke$arity$3 = (function (name,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.check_box10564.cljs$lang$maxFixedArity = 3;


sablono.core.check_box = sablono.core.wrap_attrs.call(null,sablono.core.check_box10564);
/**
 * Creates a radio button.
 */
sablono.core.radio_button10568 = (function sablono$core$radio_button10568(var_args){
var G__10570 = arguments.length;
switch (G__10570) {
case 1:
return sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$1 = (function (group){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,sablono.util.as_str.call(null,group))], null)], null);
});

sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$2 = (function (group,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,sablono.util.as_str.call(null,group)),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.radio_button10568.cljs$core$IFn$_invoke$arity$3 = (function (group,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,group)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,value))].join('')),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.radio_button10568.cljs$lang$maxFixedArity = 3;


sablono.core.radio_button = sablono.core.wrap_attrs.call(null,sablono.core.radio_button10568);
sablono.core.hash_key = (function sablono$core$hash_key(x){
return goog.string.hashCode(cljs.core.pr_str.call(null,x));
});
/**
 * Creates a seq of option tags from a collection.
 */
sablono.core.select_options10572 = (function sablono$core$select_options10572(coll){
var iter__4523__auto__ = (function sablono$core$select_options10572_$_iter__10573(s__10574){
return (new cljs.core.LazySeq(null,(function (){
var s__10574__$1 = s__10574;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__10574__$1);
if(temp__5457__auto__){
var s__10574__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10574__$2)){
var c__4521__auto__ = cljs.core.chunk_first.call(null,s__10574__$2);
var size__4522__auto__ = cljs.core.count.call(null,c__4521__auto__);
var b__10576 = cljs.core.chunk_buffer.call(null,size__4522__auto__);
if((function (){var i__10575 = (0);
while(true){
if((i__10575 < size__4522__auto__)){
var x = cljs.core._nth.call(null,c__4521__auto__,i__10575);
cljs.core.chunk_append.call(null,b__10576,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__10577 = x;
var text = cljs.core.nth.call(null,vec__10577,(0),null);
var val = cljs.core.nth.call(null,vec__10577,(1),null);
var disabled_QMARK_ = cljs.core.nth.call(null,vec__10577,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$.call(null,disabled_QMARK_);
if(cljs.core.sequential_QMARK_.call(null,val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),sablono.core.select_options10572.call(null,val)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)));

var G__10583 = (i__10575 + (1));
i__10575 = G__10583;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10576),sablono$core$select_options10572_$_iter__10573.call(null,cljs.core.chunk_rest.call(null,s__10574__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10576),null);
}
} else {
var x = cljs.core.first.call(null,s__10574__$2);
return cljs.core.cons.call(null,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__10580 = x;
var text = cljs.core.nth.call(null,vec__10580,(0),null);
var val = cljs.core.nth.call(null,vec__10580,(1),null);
var disabled_QMARK_ = cljs.core.nth.call(null,vec__10580,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$.call(null,disabled_QMARK_);
if(cljs.core.sequential_QMARK_.call(null,val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),sablono.core.select_options10572.call(null,val)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)),sablono$core$select_options10572_$_iter__10573.call(null,cljs.core.rest.call(null,s__10574__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__.call(null,coll);
});

sablono.core.select_options = sablono.core.wrap_attrs.call(null,sablono.core.select_options10572);
/**
 * Creates a drop-down box using the <select> tag.
 */
sablono.core.drop_down10584 = (function sablono$core$drop_down10584(var_args){
var G__10586 = arguments.length;
switch (G__10586) {
case 2:
return sablono.core.drop_down10584.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.drop_down10584.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.drop_down10584.cljs$core$IFn$_invoke$arity$2 = (function (name,options){
return sablono.core.drop_down10584.call(null,name,options,null);
});

sablono.core.drop_down10584.cljs$core$IFn$_invoke$arity$3 = (function (name,options,selected){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null),sablono.core.select_options.call(null,options,selected)], null);
});

sablono.core.drop_down10584.cljs$lang$maxFixedArity = 3;


sablono.core.drop_down = sablono.core.wrap_attrs.call(null,sablono.core.drop_down10584);
/**
 * Creates a text area element.
 */
sablono.core.text_area10588 = (function sablono$core$text_area10588(var_args){
var G__10590 = arguments.length;
switch (G__10590) {
case 1:
return sablono.core.text_area10588.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_area10588.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.text_area10588.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.text_area10588.cljs$core$IFn$_invoke$arity$2 = (function (name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4131__auto__ = value;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return undefined;
}
})()], null)], null);
});

sablono.core.text_area10588.cljs$lang$maxFixedArity = 2;


sablono.core.text_area = sablono.core.wrap_attrs.call(null,sablono.core.text_area10588);
/**
 * Creates a label for an input field with the supplied name.
 */
sablono.core.label10592 = (function sablono$core$label10592(name,text){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"htmlFor","htmlFor",-1050291720),sablono.core.make_id.call(null,name)], null),text], null);
});

sablono.core.label = sablono.core.wrap_attrs.call(null,sablono.core.label10592);
/**
 * Creates a submit button.
 */
sablono.core.submit_button10593 = (function sablono$core$submit_button10593(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"submit",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.submit_button = sablono.core.wrap_attrs.call(null,sablono.core.submit_button10593);
/**
 * Creates a form reset button.
 */
sablono.core.reset_button10594 = (function sablono$core$reset_button10594(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"reset",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.reset_button = sablono.core.wrap_attrs.call(null,sablono.core.reset_button10594);
/**
 * Create a form that points to a particular method and route.
 *   e.g. (form-to [:put "/post"]
 *       ...)
 */
sablono.core.form_to10595 = (function sablono$core$form_to10595(var_args){
var args__4736__auto__ = [];
var len__4730__auto___10602 = arguments.length;
var i__4731__auto___10603 = (0);
while(true){
if((i__4731__auto___10603 < len__4730__auto___10602)){
args__4736__auto__.push((arguments[i__4731__auto___10603]));

var G__10604 = (i__4731__auto___10603 + (1));
i__4731__auto___10603 = G__10604;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return sablono.core.form_to10595.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

sablono.core.form_to10595.cljs$core$IFn$_invoke$arity$variadic = (function (p__10598,body){
var vec__10599 = p__10598;
var method = cljs.core.nth.call(null,vec__10599,(0),null);
var action = cljs.core.nth.call(null,vec__10599,(1),null);
var method_str = clojure.string.upper_case.call(null,cljs.core.name.call(null,method));
var action_uri = sablono.util.to_uri.call(null,action);
return cljs.core.vec.call(null,cljs.core.concat.call(null,((cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"get","get",1683182755),null,new cljs.core.Keyword(null,"post","post",269697687),null], null), null),method))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),method_str,new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),"POST",new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null),sablono.core.hidden_field.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),(3735928559)], null),"_method",method_str)], null)),body));
});

sablono.core.form_to10595.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.form_to10595.cljs$lang$applyTo = (function (seq10596){
var G__10597 = cljs.core.first.call(null,seq10596);
var seq10596__$1 = cljs.core.next.call(null,seq10596);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__10597,seq10596__$1);
});


sablono.core.form_to = sablono.core.wrap_attrs.call(null,sablono.core.form_to10595);

//# sourceMappingURL=core.js.map
