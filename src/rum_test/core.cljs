(ns ^:figwheel-hooks rum-test.core
  (:require
   [goog.dom :as gdom]
   [rum.core :as rum]
   [jayq.core :as jayq :refer [$]]
   [sablono.util]))

(defonce app-state (atom {:text "Ananya's Rum Testing" :counter 0}))

;; define your app data so that it doesn't get over-written on reload
(defonce is-element-dropped? (atom false))

(defn attrs [a]
      (clj->js (sablono.util/html-to-dom-attrs a)))

(defn get-app-element []
  (gdom/getElement "app"))


(defn add-like []
  (swap! app-state update-in [:counter] inc))

(rum/defc person [name likes]
  [:div {:class "person"
          :on-click #(add-like)}
          (str "Welcome to the app! " "Click me! " name " is liked " likes " times ")])

(rum/defc like-counter < rum/reactive []
  [:div {}
    (person "Ananya" (:counter (rum/react app-state)))])    

(rum/defc tile [text number]
          [:div {:class "tile" :id number} text])

(rum/defc tiles []
          [:.tiles {}
           (tile "Drop the red box in here" 1)])

(rum/defc dropped-message < rum/reactive []
          [:div {}
           (str "Has an element been dropped? " (rum/react is-element-dropped?))])

(rum/defc card [number]
          [:.card {:data-number number
                   :id number}])

(rum/defc cards []
          [:.cards {}
           (card 1)])

(rum/defc content []
          [:div {}
           (like-counter) 
           (tiles)
           (cards)
           (dropped-message)])

(defn mount [el]
  (rum/mount (content) el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

(defn make-draggable []
      (.draggable ($ ".card") (attrs {:revert true :cursor "move"})))

(defn handle-drop [event ui]
      (let [draggable-id (jayq/data (.-draggable ui) "number")]
           (println "Dropping element with id" draggable-id)
           (reset! is-element-dropped? true)
           (.draggable (.-draggable ui) "disable")
           (.droppable ($ (str "#" (.-id (.-target event)))) "disable")
           (.position (.-draggable ui)
                      (attrs {:of ($ (str "#" (.-id (.-target event)))) :my "left top" :at "left top"}))))

(defn start-dragging [event ui]
      (reset! is-element-dropped? false))

(defn make-droppable []
      (.droppable ($ (str ".tile"))
                  (attrs {:hoverClass "hovered-tile" :drop handle-drop :activate start-dragging})))

(mount-app-element)
(make-draggable)
(make-droppable)

(defn ^:after-load on-reload []
  (mount-app-element)
  (make-draggable)
  (make-droppable))